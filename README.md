# Snake

Simple Snake game

## Installation

Requirements in [requirements.txt](requirements.txt) file

## Running
```bash
python main.py
```
## Technologies
* python 3.5
* pygame 1.9

## Author
Pawel Sucholbiak <pawel.sucholbiak@gmail.com>
\
[LinkedIn Pawel Sucholbiak](https://www.linkedin.com/in/pawelsucholbiak)