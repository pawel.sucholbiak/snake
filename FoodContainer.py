from Food import Food


class FoodContainer(object):
    def __init__(self, food_count=1, food_size=10, max_game_point=(0, 0)):
        self.food_count = food_count
        self.food_size = food_size
        self.max_game_point = max_game_point
        self.food_list = [
            Food().set_square_size(10).set_absolute_max_position(self.max_game_point).set_random_position()
            for _ in range(food_count)]

    def draw_food_on(self, canvas):
        for food in self.food_list:
            food.draw_on(canvas)
